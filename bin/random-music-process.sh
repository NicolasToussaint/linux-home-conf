#!/bin/sh

#cp -L ...

step_1() {
	for f in *;
    do
        ff=$(echo "$f" | sed 's/^[0-9 -\.]*//;s/MP3$/mp3/' | tr -d "[]{}<>%#/\\-'" | iconv -t ascii//translit)
        [ "$f" = "$ff" ] || mv -v "$f" "$ff"
    done
}

step_2() {
for e in flac ogg m4a mpc wma FLAC OGG M4A MPC WMA
do
	if ls *$e >/dev/null 2>&1
	then
		for f in *$e
		do
			ff=$(echo "$f" | sed "s/\.$e//")
			echo "===== $f => $ff.mp3"
			ffmpeg -loglevel error -i "$f" "$ff.mp3" || exit 1
			rm "$f"
		done
	fi
done
}

f_get_tag() {
    ffprobe -loglevel error -show_entries format_tags=$2 -of default=noprint_wrappers=1:nokey=1 "$1" | cut -b1-$3
}

step_4() {
    tmp_file=$(mktemp)
    for f in *mp3
    do
        prefix=$(echo "$(date)$f" | md5sum | cut -b1-2)
        artist=$(f_get_tag "$f" "artist" 16)
        title=$(f_get_tag "$f" "title" 48)
        new_1="$f"
        if [ -n "$artist$title" ]
        then
            new_1="${artist}_${title}"
        fi
        new_2=$(echo "${prefix}_$new_1" | tr -d "[]{}<>%#/\\-'" | iconv -t ascii//translit)
    	mv -v "$f" "$new_2"
        echo "$f|$new_2" >>$tmp_file
    done 
    column -t -s '|' $tmp_file
}

step_1
step_2
step_4
du -h .
