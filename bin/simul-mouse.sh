#!/bin/sh

# https://askubuntu.com/questions/1217597/mousemover-to-prevent-idle

if ! which evemu-event >/dev/null 2>&1
then
	echo "Install evemu-tools: sudo apt install evemu-tools"
	exit 1
fi

mouse_device=$(ls -1 /dev/input/by-id/*event-mouse | tail -1)
[ -z "${mouse_device}" ] && { echo "Cannot find mouse event device" ; exit 1 ; }

logger -s "Using ${mouse_device}"

while true
do
    if test -e $mouse_device
    then
        /usr/bin/evemu-event ${mouse_device} --type EV_REL --code REL_X --value 2 --sync
        /usr/bin/evemu-event ${mouse_device} --type EV_REL --code REL_X --value -2 --sync
        echo -n "#"
    else
        echo -n "x"
    fi
    sleep 60
done

