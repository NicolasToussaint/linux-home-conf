#!/bin/sh

youtube_dl_cmd=/tmp/yt-dlp
out_dir="$(pwd)"
mode=video

f_usage() {
	echo "HELP HERE."
	exit $1
}

f_fatal() {
	echo "Failure: $*"
	echo
	exit 1
}

while getopts "t:o:u:f:av" opt; do
    case $opt in
    t)  export TMPDIR=$(realpath $OPTARG) ;;
    o)  out_dir=$(realpath $OPTARG) ;;
    u)  url=$OPTARG ;;
    f)  file=$OPTARG ;;
    a)  mode="audio" ;;
    v)  mode="video" ;;
    h)  f_usage 0 ;;
    \?) f_usage 1 ;;
  esac
done

if [ -n "$TMPDIR" ]
then
    mkdir -p "$TMPDIR" || f_fatal "Cannot create base tmp dir '$TMPDIR'"
fi

if [ -n "$out_dir" ]
then
    mkdir -p "$out_dir" || f_fatal "Cannot make dir '$out_dir'"
fi

[ -n "$url$file" ] || f_fatal "Missing url or file arg"
if [ ! -x $youtube_dl_cmd ]
then
	curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o $youtube_dl_cmd
	chmod a+x $youtube_dl_cmd
fi
[ -x $youtube_dl_cmd ] || f_fatal "Failed to download tool"

f_do_download() {
    case "$2" in
        audio)
            format_options="--extract-audio --audio-format mp3"
            ;;
        video) format_options="" ;;
        *) f_fatal "Wrong mode '$2'"
    esac
    url="$1"
    echo "Get output filename"
	out_dl=$($youtube_dl_cmd --restrict-filenames --update --get-filename  $format_options "$url") || return 1
	out_base=$(echo "$out_dl" | sed 's/\.[^\.]*$//')
    tmp_dir=$(mktemp -d -t ytd.XXXX)
    cur_dir=$(pwd)

cat <<EOS
======================================
extra option: $youtube_dl_extra_options
mode    : '$2'
url     : '$url'
out_dl  : '$out_dl'
out_base: '$out_base'
out_dir : '$out_dir'
tmp_dir : '$tmp_dir'
------------------
EOS

    cd "$tmp_dir" || f_fatal "Failed to chdir to '$tmp_dir'"
    echo ======================================
	echo "$youtube_dl_cmd --restrict-filenames --update $format_options $youtube_dl_extra_options \"$url\""
    echo ======================================
    $youtube_dl_cmd --restrict-filenames --update $format_options $youtube_dl_extra_options "$url"
    rc=$?
    echo ======================================
    if [ $rc -eq 0 ]
    then
        out_real=$(ls -1 $(echo "$out_base*" | sed 's!\([]\[]\)!\\\1!g'))
    	echo "x out_real: '$out_real'"
        if [ -z "$out_real" ]
        then
            echo "ERROR: no file downloaded: ($out_real)"
            rc=2
        else
            mv -v "$out_real" "$out_dir/"
        fi
    else
        echo "Download failed"
    fi
    cd "$cur_dir"
    return $rc
}

if [ -n "$file" ]
then
    echo "Download from file: $file"
    i=0
    rc_ok=0
    rc_nok=0
    cat "$file" | while read _url
    do
        i=$((i+1))
        echo "===== URL #$i: $_url"
        if f_do_download "$_url" "$mode"
        then
            rc_ok=$((rc_ok+1))
            echo "=   = URL #$i: DOWNLOAD OK"
        else
            rc_nok=$((rc_nok+1))
            echo "=   = URL #$i: DOWNLOAD FAILED"
        fi
    done
    echo "Processed $i links"
    echo "Success: $rc_ok"
    echo "Failure: $rc_nok"
else
    echo "Download from unique URL: $url"
    f_do_download "$url" "$mode"
    rc=$?
fi

echo "--- --- ---"

