#!/bin/sh

f_usage() {
cat <<EOS

$(basename $0) [options|help]

Options:
  notime        : date only
  time          : show time
  time_sec      : show time with seconds
  pretty        : prettier, non sortable output
  epoch         : only show seconds since the epoch
  cmd, command  : show `date` command
  cb, clipboard : paste result in copy&paste clipboard

EOS
  exit $1
}

[ $# -eq 0 ] && set time_sec cmd

format='%Y-%m-%d'

for p in "$@"
do
  case $p in
  notime) format_time="" ;;
  time) format_time=" %H:%M" ;;
  time_sec) format_time=" %H:%M:%S" ;;
  pretty) 
    pfx=''
    sfx=''
    format='%a %d-%m-%Y'
    ;;
  epoch) format="%s" ;;
  cmd|command) print_command=true ;;
  cb|clipboard) print_clipboard=true ;;
  help|-h|--help) f_usage 0;;
  *) f_usage 1 ;;
  esac
done
      
full_format="+$pfx$format$format_time$sfx"
if [ "$print_command" = "true" ]
then
  echo ">> date '$full_format'"
fi

date "$full_format"

if [ "$print_clipboard" = "true" ]
then
  echo -n $(date "$full_format") | xclip -selection clipboard
fi

