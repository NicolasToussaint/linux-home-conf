#!/bin/sh


current_target="current"
ref="$1"
version=$2

#ref="https://github.com/ytdl-org/youtube-dl/releases/download/2021.02.10/youtube-dl"

github_url="$(echo $ref | sed 's_/releases.*__')"
github_url_release="$(echo $ref | sed 's_\(.*download/[^0-9]*\).*_\1_')"
ref_version="$(echo $ref | sed 's_.*/download/[^0-9]*\([0-9\.]*\)/.*$_\1_')"
ref_release=$(echo $ref | sed "s!.*$ref_version/!!")


if [ -z "$version" ]
then
    version=$(git ls-remote --tags $github_url.git | grep -v '\^' | sed 's_.*/[^0-9]*__' | tail -n 1)
else
    echo "Current Version: $(ls -l $current_target | sed "s_.* -> __")"
    echo "Version Suggestion:"
    git ls-remote --tags $github_url.git | grep -v '\^' | sed 's_.*/[^0-9]*__' | tail -n 5
    echo 
fi

release_file_pre=$(echo $ref_release | sed "s!$ref_version.*!!")
release_file_pst=$(echo $ref_release | sed "s!.*$ref_version!!")
release_file="${release_file_pre}${version}${release_file_pst}"
gh_download_url="$github_url_release${version}/${release_file}"

cat <<EOS
<<<<<
github_url: $github_url
github_url_release: $github_url_release
ref_version: $ref_version
ref_release: $ref_release
release_file_pre: $release_file_pre
release_file_pst: $release_file_pst
release_file: ${release_file_pre}${version}${release_file_pst}

Github Download URL: $gh_download_url
>>>>>

EOS

f_fatal() {
        echo $*
        exit 1
}


echo "Download: ${release_file}"

wget --show-progress $gh_download_url || f_fatal "wget failed"
tarball=$(basename "$gh_download_url")
directory=$(tar tf $tarball | head -n 1 | sed 's_/$__') || f_fatal "Bug"

echo "Directory: $directory"
tar xf $tarball
ln -nfs $directory $current_target

