#!/bin/sh

f_fatal() {
	echo "Error: $*"
	exit 1
}

f_check_file() {
	[ -r "$1" ] || f_fatal echo "Cannot find file '$1'"
}

dump_dir=/tmp/dump/$(basename $0 .sh)_$(date +%Y%m%d_%H%M%S)

for f in cuebreakpoints shnsplit
do 
    which $f >/dev/null 2>&1 || f_fatal "Tool '$f' missing (install shntool)"
done

auto=
if [ "$1" = "auto" ]
then
	auto=yes
	shift
fi

if [ -d "$1" ]
then
    echo "Change Dir to '$1'"
    cd "$1"
    shift
fi

if [ -n "$1" ]
then
   	cue="$1"
else
	cue=$(ls -1 *.cue | head -n 1)
fi

flac=$(basename "$cue" .cue).flac
log=$(basename "$cue" .cue).log

f_check_file "$cue"
f_check_file "$flac"

cat <<EOS

$(basename $0)

CUE : [$cue]
FLAC: [$flac]

EOS

[ "$auto" = "yes" ] || read -p "Continue?" a

cuebreakpoints "$cue" | shnsplit -o flac "$flac"  || f_fatal "no luck"
echo
n=0
sed -n '/  *TITLE/s/^[ TILE]*"\([^"]*\)"/\1/p'  "$cue" | tr -d '\r'  | tr '/' '-' | \
	while read title;
	do
		n=$((n+1))
		num=$(printf "%.02d" $n)
		echo -n "$num : "
		mv -v "split-track${num}.flac" "$num - ${title}.flac" || f_fatal "no luck"
	done

echo
echo "Moving original files to '$dump_dir'"

mkdir -p "$dump_dir" || f_fatal "no luck"
for f in "$cue" "$flac" "$log"
do
	[ -w "$f" ] && mv -v "$f" "$dump_dir/$f"
done

