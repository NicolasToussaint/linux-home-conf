#!/bin/sh

ref_id_file="/tmp/$(basename $0).id"
ref_md_file="/tmp/$(basename $0)-md-link"

f_fatal() {
    echo "$@"
    exit 1
}

f_init() {
    local id=0
    [ -n "$1" ] && id=$1
    echo $id >$ref_id_file || f_fatal
    ref_if=$id
}

f_process() {
    local ref=$1
    link="$(xclip -o)"
    text="[$ref] $link"
    echo "$text" | xclip -selection clipboard
    notify-send "clipboard - Reference" "ref: $ref\nlnk: $link"
}

touch "$ref_id_file" || f_fatal
ref_id=$(head -n 1 $ref_id_file | grep '^[0-9][0-9]*$')
[ -z "$ref_id" ] && f_init

# DEBUG
for t in primary secondary clipboard buffer-cut
do
    echo "--- $t :"
    xclip -o -selection $t 2>/dev/null
    echo
done



if [ "$1" = "-m" ]
then # Markdown link
    cb="$(xclip -o -selection clipboard | tr -d '\n' | sed 's!^ *!!')"
    kw_lnk="lnk:"
    kw_ref="ref:"
    if echo "$cb" | grep -q "^\[.*]\(.*\)$"
    then
        echo "Clipboard already in MD Format"
        lnk=$(sed -n "/^${kw_lnk}/s!^${kw_lnk}!!p" $ref_md_file)
        ref=$(sed -n "/^${kw_ref}/s!^${kw_ref}!!p" $ref_md_file)
    else
        if echo "$cb" | grep -q '^http'
        then
            kw=${kw_lnk}
        else
            kw=${kw_ref}
        fi
        [ -s $ref_md_file ] && sed -i "/^${kw}/d" $ref_md_file
        echo "${kw}$cb" >> $ref_md_file
        lnk=$(sed -n "/^${kw_lnk}/s!^${kw_lnk}!!p" $ref_md_file)
        ref=$(sed -n "/^${kw_ref}/s!^${kw_ref}!!p" $ref_md_file)
        echo -n "[$ref]($lnk)" | xclip -selection clipboard
    fi
    echo ===
    echo "[$ref]($lnk)"
    echo ===
    notify-send "clipboard - MarkDown link" "ref: $ref\nlnk: $lnk"
elif [ "$1" = "-0" ]
then
    f_init
    f_process 0
elif [ "$1" = "-p" ]
then
    sleep 0.4
    xdotool type --delay 300 "[$ref_id]"
else
    ref_id=$((ref_id+1))
    f_process $ref_id
    f_init $ref_id
fi

