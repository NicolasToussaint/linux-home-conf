#!/bin/bash

# https://ghostscript.readthedocs.io/en/gs10.0.0/VectorDevices.html#controls-and-features-specific-to-postscript-and-pdf-input

# -dPDFSETTINGS=configuration
# Presets the "distiller parameters" to one of four predefined settings:
#  /screen   selects low-resolution output similar to the Acrobat Distiller "Screen Optimized" setting#  /ebook    selects medium-resolution output similar to the Acrobat Distiller "eBook" setting.
#  /printer  selects output similar to the Acrobat Distiller "Print Optimized" setting.
#  /prepress selects output similar to Acrobat Distiller "Prepress Optimized" setting.
#  /default  selects output intended to be useful across a wide variety of uses,
#            possibly at the expense of a larger output file.


min_pdf_size="1M"
debug=true

mode=default
mode=printer
suffix="gs-$mode"

f_debug() {
    if [ -n "$1" ]
    then
        test "$debug" = "true" && echo "$@"
    else
        test "$debug" = "true" 
    fi
}

f_log() {
    echo "$@"
}

f_fatal() {
    f_log "ERROR: $@"
    exit 1
}

f_resize() {
    file_in="$1"
    file_orig=$(dirname "$file_in")/$(basename "$file_in" .pdf).orig.pdf
    file_new=$(dirname "$file_in")/$(basename "$file_in" .pdf).$suffix.pdf

    f_debug "Resize PDF: file in '$file_in'"
    f_debug "          : orig -> '$file_orig'"
    f_debug "          : new  -> '$file_new'"
    test -e "$file_in" || f_fatal "Error processing '$file_in'"

    gs -sDEVICE=pdfwrite -dPDFSETTINGS=/$mode -q -o "$file_new" "$file_in"
    test -e "$file_new" || f_fatal "Error generating '$file_new'"
    mv "$file_in" "$file_orig"
    f_debug && du -h "$file_orig" "$file_new"
}

f_resize_loop(){
    while read f
    do f_resize "$f"
    done
}


for f in "$@"
do
    if ! test -e "$f"
    then
        ls "$f"
        f_log "Cannot find "$f", skipping"
    else
        find "$f" -not -name '*.orig.pdf' -not -name '*.gs-*.pdf' -name '*.pdf' \
            -size +$min_pdf_size | f_resize_loop
    fi
done

