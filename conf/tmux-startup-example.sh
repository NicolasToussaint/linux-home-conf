#!/bin/sh

. ~/linux-home-conf/conf/tmux-startup_stub.sh

# = = = = = = = = = = = = = = = = = = =
f_session 0
f_window "win" -c ~/
f_send_keys "./command.sh" C-m

f_split_window -c /dir1
f_split_window -h -c /dir2

tmux select-window -t $SESSION:0
